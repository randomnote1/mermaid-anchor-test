# Example Page

## Markdown links

- [A](#a)
- [B](#b)
- [C](#c)
- [D](#d)

## Mermaid diagram

```mermaid
flowchart LR;
A --> B
B --> C
C --> D

click A "#a"
click B "#b"
click C "#c"
click D "#d"
```

## A

"A" content goes here.

## B

"B" content goes here.

## C

"C" content goes here.

## D

"D" content goes here.
